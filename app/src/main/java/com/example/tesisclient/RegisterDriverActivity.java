package com.example.tesisclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.VideoView;


import com.example.tesisclient.entities.driver;
import com.example.tesisclient.response.responsePostDriver;
import com.example.tesisclient.retrofit.apiRest;
import com.example.tesisclient.retrofit.utilities;
import com.google.gson.Gson;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterDriverActivity extends AppCompatActivity {
    private apiRest mApiService;
    ScrollView ScrollView1,ScrollView2,ScrollView3;
    EditText user;
    EditText name;
    EditText appat;
    EditText amat;
    EditText phone;
    EditText email;
    EditText edad;
    EditText cp;
    EditText calle;
    EditText colony;
    EditText municipality;
    EditText noext;
    EditText noint;
    Button btnAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_driver);



        ScrollView1=(ScrollView) findViewById(R.id.ScrollView1);
        ScrollView2=(ScrollView) findViewById(R.id.ScrollView2);
        ScrollView3=(ScrollView) findViewById(R.id.ScrollView3);
        user=(EditText) findViewById(R.id.EditTextuser);
        name=(EditText) findViewById(R.id.EditTextName);
        appat=(EditText) findViewById(R.id.EditTextAppat);
        amat=(EditText) findViewById(R.id.EditTextAmat);
        edad=(EditText) findViewById(R.id.EditTextEdad);
        phone=(EditText) findViewById(R.id.EditTextPhone);
        email=(EditText) findViewById(R.id.EditTextEmail);
        cp=(EditText) findViewById(R.id.EditTextCp);
        calle=(EditText) findViewById(R.id.EditTextCalle);
        colony=(EditText) findViewById(R.id.EditTextColony);
        municipality=(EditText) findViewById(R.id.EditTextMunicipality);
        noext=(EditText) findViewById(R.id.EditTextNoExt);
        noint=(EditText) findViewById(R.id.EditTextNoInt);
        mApiService= utilities.getApiService();
        btnAdd = findViewById(R.id.buttonRegisterVisible);


    }


    public void onClick(View view){
            String despedida=", Por favor verifíquelo.";

        switch (view.getId()){
        //botones de siguiente
            case R.id.buttonNext2:
                if(this.user.getText().toString().equals("")){
                    Toast.makeText(this, "El número de matricula no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return; }
                if(this.name.getText().toString().equals("")){
                    Toast.makeText(this, "Nombre no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                if(this.appat.getText().toString().equals("")){
                    Toast.makeText(this, "El Apellido Paterno no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                if(this.amat.getText().toString().equals("")){
                    Toast.makeText(this, "El Apellido Materno no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }

                ScrollView1.setVisibility(View.INVISIBLE);
                ScrollView2.setVisibility(View.VISIBLE);
                break;

            case R.id.buttonNext3:
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if(this.edad.getText().toString().equals("")){
                    Toast.makeText(this, "La edad no puede estar vacía"+despedida, Toast.LENGTH_SHORT).show();
                    return; }
                if(Integer.parseInt(this.edad.getText().toString())<15){
                    Toast.makeText(this, "Tienes que tener una edad mayor a 15 años para poder regístrarte"+despedida, Toast.LENGTH_SHORT).show();
                    return; }
                if(Integer.parseInt(this.edad.getText().toString())>100){
                    Toast.makeText(this, "Escoge una edad valida por favor"+despedida, Toast.LENGTH_SHORT).show();
                    return; }
                //valid phone
                if(Pattern.matches("[a-zA-Z]+", this.phone.getText().toString())) {
                    Toast.makeText(this, "El número telefónico lleva solo números"+despedida, Toast.LENGTH_SHORT).show();
                   return;
                }
                if(phone.getText().toString().length() < 6 || phone.getText().toString().length() >= 13) {
                    Toast.makeText(this, "Longitud de número de teléfono invalida"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                //Valid email
                String emailvalid = this.email.getText().toString().trim();

                if (!emailvalid.matches(emailPattern))
                {
                    Toast.makeText(getApplicationContext(),"Dirección de correo invalida"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                //CP
                if(this.cp.getText().toString().length()== 4 ) {
                    Toast.makeText(this, "Longitud de código postal invalida"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }


                ScrollView2.setVisibility(View.INVISIBLE);
                ScrollView3.setVisibility(View.VISIBLE);

                break;
            //botones de back
            case R.id.buttonBack1:
                ScrollView2.setVisibility(View.INVISIBLE);
                ScrollView1.setVisibility(View.VISIBLE);
                break;

            case R.id.buttonBack2:
                ScrollView3.setVisibility(View.INVISIBLE);
                ScrollView2.setVisibility(View.VISIBLE);
                break;

            //boton de acetar para registarse
            case R.id.buttonRegister:
                Toast.makeText(getApplicationContext(),"Enviando",Toast.LENGTH_SHORT).show();
                //municipio
                if(this.municipality.getText().toString().equals("")) {
                    Toast.makeText(this, "El campo municipio no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                //calle
                if(this.calle.getText().toString().equals("")) {
                    Toast.makeText(this, "El campo calle no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                //colonia
                if(this.calle.getText().toString().equals("")) {
                    Toast.makeText(this, "El campo calle no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                //No
                if(this.noint.getText().toString().length()== 0 ) {
                    noint.setText("0");
                    return;
                }
                if(this.noext.getText().toString().length()== 0 ) {
                    Toast.makeText(this, "El no exterior no puede estar vació"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Integer.parseInt(this.noint.getText().toString())< 0 ) {
                    Toast.makeText(this, "No interior invalido"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }
                if( Integer.parseInt(this.noext.getText().toString())< 0 ) {
                    Toast.makeText(this, "No exterior invalido"+despedida, Toast.LENGTH_SHORT).show();
                    return;
                }


                //formato completo de fecha
                mApiService.addDriver(new driver(null, user.getText().toString(), name.getText().toString(), appat.getText().toString(), amat.getText().toString(), phone.getText().toString(),this.email.getText().toString(), edad.getText().toString(), Integer.parseInt(cp.getText().toString()), calle.getText().toString(), colony.getText().toString(), municipality.getText().toString(), Integer.parseInt(noext.getText().toString()), Integer.parseInt(noint.getText().toString()), null)).enqueue(new Callback<responsePostDriver>() {
                    @Override
                    public void onResponse(Call<responsePostDriver> call, Response<responsePostDriver> response) {
                        Gson objectConsole = new Gson();
                        Log.i("pruebaRest", objectConsole.toJson(response.body()));
                        int codeResp=Integer.parseInt(objectConsole.toJson(response.body().getStatus().getCode()));
                        String tokenResp=objectConsole.toJson(response.body().getStatus().getMessage());
                        //quitarle las comillas que trae el string al principio y al final en el caso de node

                        if(codeResp==1){
                            // aqui debemos guardar el token y los datos que regresan en shared preferences
                            SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
                            SharedPreferences.Editor objEditor = preferences.edit();

                            objEditor.putBoolean("tokenExists",true);
                            objEditor.putString("idUsuario",tokenResp);
                            objEditor.putString("user",user.getText().toString());

                            objEditor.putString("name",name.getText().toString());
                            objEditor.putString("appat",appat.getText().toString());
                            objEditor.putString("amat",amat.getText().toString());

                            objEditor.putString("phone",phone.getText().toString());
                            objEditor.putString("email",email.getText().toString());
                            objEditor.putString("edad",edad.getText().toString());

                            objEditor.putInt("cp",Integer.parseInt(cp.getText().toString()));
                            objEditor.putString("calle",calle.getText().toString());
                            objEditor.putString("colony",colony.getText().toString());

                            objEditor.putString("municipality",municipality.getText().toString());
                            objEditor.putInt("noext",Integer.parseInt(noext.getText().toString()));
                            objEditor.putInt("noint",Integer.parseInt(noint.getText().toString()));


                            objEditor.commit();
                            //guardar el id de usuario con el valor por default


                            Toast.makeText(getApplicationContext(),"Iniciando Sesión "+tokenResp,Toast.LENGTH_SHORT).show();

                            //redireccionar a Main activity
                            Intent miIntent=null;
                            miIntent=new Intent(RegisterDriverActivity.this,MainActivity.class);
                            startActivity(miIntent);
                        }else{

                            Toast.makeText(getApplicationContext(),tokenResp,Toast.LENGTH_SHORT).show();

                        }


                    }

                    @Override
                    public void onFailure(Call<responsePostDriver> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),
                                "Lo sentimos pero no pudimos conectar el servidor\nVerifique su conexión e inténtelo nuevamente.", Toast.LENGTH_SHORT).show();
                    }
                });




        }


    }

 //   @Override
  //  public void onBackPressed() {
// super.onBackPressed();
// Not calling **super**, disables back button in current screen.
 //   }

}
