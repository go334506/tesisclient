package com.example.tesisclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.tesisclient.entities.driver;
import com.example.tesisclient.response.responsePostDriver;
import com.google.gson.Gson;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelloActivity extends AppCompatActivity {
    VideoView videoViewWelcome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);
        Uri path = Uri.parse("android.resource://com.example.tesisclient/"
                + R.raw.video);
        videoViewWelcome= (VideoView) findViewById(R.id.videoViewWelcome);
        videoViewWelcome.setVideoURI(path);
        videoViewWelcome.start();
    }
    public void onClick(View view){
        Intent miIntent=null;
        switch (view.getId()){
            case R.id.buttonLogin://redireccionar a login activity

                miIntent=new Intent(HelloActivity.this,LoginActivity.class);

                break;
            case R.id.buttonRegisterVisible://redireccionar a login activity
                miIntent=new Intent(HelloActivity.this,RegisterDriverActivity.class);
                break;

        }
        if(miIntent!=null){
            startActivity(miIntent);
        }


    }
}
