package com.example.tesisclient.retrofit;


import com.example.tesisclient.entities.driver;
import com.example.tesisclient.entities.report;
import com.example.tesisclient.response.reponsePostReport;
import com.example.tesisclient.response.responsePostDriver;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

//endpoints
public interface apiRest {


    @POST("driver/create")//crear un conductor
    Call<responsePostDriver> addDriver(@Body driver driver);

    @POST("driver/login")//driver/login
    Call<responsePostDriver> loginDriver(@Body driver driver);

    @POST("report/create")
    Call<reponsePostReport> addReport(@Body report report);





}
