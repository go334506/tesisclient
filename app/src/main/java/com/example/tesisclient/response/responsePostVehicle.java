package com.example.tesisclient.response;
import com.example.tesisclient.entities.status;
import com.example.tesisclient.entities.vehicle;

public class responsePostVehicle {
    status status;
    vehicle vehicle;

    public responsePostVehicle(com.example.tesisclient.entities.status status, com.example.tesisclient.entities.vehicle vehicle) {
        this.status = status;
        this.vehicle = vehicle;
    }

    public com.example.tesisclient.entities.status getStatus() {
        return status;
    }

    public void setStatus(com.example.tesisclient.entities.status status) {
        this.status = status;
    }

    public com.example.tesisclient.entities.vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(com.example.tesisclient.entities.vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
