package com.example.tesisclient.response;


import com.example.tesisclient.entities.driver;
import com.example.tesisclient.entities.status;

import java.util.List;

public class responseGetDrivers {
    status status;
    List<driver> drivers;

    public responseGetDrivers(com.example.tesisclient.entities.status status, List<driver> drivers) {
        this.status = status;
        this.drivers = drivers;
    }

    public com.example.tesisclient.entities.status getStatus() {
        return status;
    }

    public void setStatus(com.example.tesisclient.entities.status status) {
        this.status = status;
    }

    public List<driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<driver> drivers) {
        this.drivers = drivers;
    }
}
