package com.example.tesisclient.response;
import com.example.tesisclient.entities.report;
import com.example.tesisclient.entities.status;

public class reponsePostReport {
    status status;
    report report;

    public reponsePostReport(com.example.tesisclient.entities.status status, com.example.tesisclient.entities.report report) {
        this.status = status;
        this.report = report;
    }

    public com.example.tesisclient.entities.status getStatus() {
        return status;
    }

    public void setStatus(com.example.tesisclient.entities.status status) {
        this.status = status;
    }

    public com.example.tesisclient.entities.report getReport() {
        return report;
    }

    public void setReport(com.example.tesisclient.entities.report report) {
        this.report = report;
    }
}
