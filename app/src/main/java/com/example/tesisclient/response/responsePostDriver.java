package com.example.tesisclient.response;
import com.example.tesisclient.entities.driver;
import com.example.tesisclient.entities.status;

import java.util.List;

public class responsePostDriver {
     status status;
     driver driver;

    public responsePostDriver(com.example.tesisclient.entities.status status, com.example.tesisclient.entities.driver driver) {
        this.status = status;
        this.driver = driver;
    }

    public com.example.tesisclient.entities.status getStatus() {
        return status;
    }

    public void setStatus(com.example.tesisclient.entities.status status) {
        this.status = status;
    }

    public com.example.tesisclient.entities.driver getDriver() {
        return driver;
    }

    public void setDriver(com.example.tesisclient.entities.driver driver) {
        this.driver = driver;
    }
}
