package com.example.tesisclient.response;
import com.example.tesisclient.entities.status;
import com.example.tesisclient.entities.medical;

public class responsePostMedical {
    status status;
    medical medical;

    public responsePostMedical(com.example.tesisclient.entities.status status, com.example.tesisclient.entities.medical medical) {
        this.status = status;
        this.medical = medical;
    }

    public com.example.tesisclient.entities.status getStatus() {
        return status;
    }

    public void setStatus(com.example.tesisclient.entities.status status) {
        this.status = status;
    }

    public com.example.tesisclient.entities.medical getMedical() {
        return medical;
    }

    public void setMedical(com.example.tesisclient.entities.medical medical) {
        this.medical = medical;
    }
}
