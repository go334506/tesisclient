package com.example.tesisclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tesisclient.entities.driver;
import com.example.tesisclient.response.responsePostDriver;
import com.example.tesisclient.retrofit.apiRest;
import com.example.tesisclient.retrofit.utilities;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private apiRest mApiService;
    EditText user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mApiService= utilities.getApiService();
        user=(EditText) findViewById(R.id.EditTextUserLogin);
    }

    public void onClick(View view){
        Intent miIntent=null;
        switch (view.getId()){
            case R.id.buttonLogin://redireccionar a login activity
                mApiService.loginDriver(new driver(null, user.getText().toString(), null, null, null, null,null, null, 0,null,null, null,0, 0, null)).enqueue(new Callback<responsePostDriver>() {
                    @Override
                    public void onResponse(Call<responsePostDriver> call, Response<responsePostDriver> response) {
                        Gson objectConsole = new Gson();
                        Log.i("pruebaRest", objectConsole.toJson(response.body()));
                        int codeResp=Integer.parseInt(objectConsole.toJson(response.body().getStatus().getCode()));
                        String tokenResp=objectConsole.toJson(response.body().getDriver().get_id());
                        //quitarle las comillas que trae el string al principio y al final en el caso de node

                        if(codeResp==1){
                            // aqui debemos guardar el token y los datos que regresan en shared preferences
                            SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
                            SharedPreferences.Editor objEditor = preferences.edit();
                            objEditor.putBoolean("tokenExists",true);
                            objEditor.putString("idUsuario",tokenResp);


                            objEditor.commit();
                            Toast.makeText(getApplicationContext(),"Iniciando Sesión ",Toast.LENGTH_SHORT).show();

                            //redireccionar a Main activity
                            Intent miIntent=null;
                            miIntent=new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(miIntent);
                        }
                        if (codeResp==-99){

                            Toast.makeText(getApplicationContext(),"Error No. de Licencia Incorrecto",Toast.LENGTH_SHORT).show();


                        }

                    }

                    @Override
                    public void onFailure(Call<responsePostDriver> call, Throwable t) {

                        Toast.makeText(getApplicationContext(),
                                "Lo sentimos pero no pudimos conectar con el servidor\nVerifique su conexión e inténtelo nuevamente.", Toast.LENGTH_SHORT).show();
                    }
                });


                //miIntent=new Intent(HelloActivity.this,LoginActivity.class);

                break;


        }
        if(miIntent!=null){
            startActivity(miIntent);
        }


    }
}
