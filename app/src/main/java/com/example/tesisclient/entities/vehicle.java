package com.example.tesisclient.entities;

public class vehicle {
    String _id;
    String idDriver;
    String enrollment;//Matrícula
    String brand;
    String model;
    String color;
    int year;
    String country;
    String nEngine;
    String nChassis;
    String __v;

    public vehicle(String _id, String idDriver, String enrollment, String brand, String model, String color, int year, String country, String nEngine, String nChassis, String __v) {
        this._id = _id;
        this.idDriver = idDriver;
        this.enrollment = enrollment;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.year = year;
        this.country = country;
        this.nEngine = nEngine;
        this.nChassis = nChassis;
        this.__v = __v;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(String enrollment) {
        this.enrollment = enrollment;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getnEngine() {
        return nEngine;
    }

    public void setnEngine(String nEngine) {
        this.nEngine = nEngine;
    }

    public String getnChassis() {
        return nChassis;
    }

    public void setnChassis(String nChassis) {
        this.nChassis = nChassis;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }
}

