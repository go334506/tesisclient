package com.example.tesisclient.entities;

public class report {
    String idDriver;
    double velocity;
    String metricUnit;
    double latitud;
    double longitud;
    double x;
    double y;
    double z;

    public report(String idDriver, double velocity, String metricUnit, double latitud, double longitud, double x, double y, double z) {
        this.idDriver = idDriver;
        this.velocity = velocity;
        this.metricUnit = metricUnit;
        this.latitud = latitud;
        this.longitud = longitud;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    public String getMetricUnit() {
        return metricUnit;
    }

    public void setMetricUnit(String metricUnit) {
        this.metricUnit = metricUnit;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
}
