package com.example.tesisclient.entities;

public class medical {

    String _id;
    String idDriver;
    String tipeBlood;
    String disease;
    String treatment;
    String surgeries;
    String transplant;
    boolean donor;
    String __v;

    public medical(String _id, String idDriver, String tipeBlood, String disease, String treatment, String surgeries, String transplant, boolean donor, String __v) {
        this._id = _id;
        this.idDriver = idDriver;
        this.tipeBlood = tipeBlood;
        this.disease = disease;
        this.treatment = treatment;
        this.surgeries = surgeries;
        this.transplant = transplant;
        this.donor = donor;
        this.__v = __v;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getTipeBlood() {
        return tipeBlood;
    }

    public void setTipeBlood(String tipeBlood) {
        this.tipeBlood = tipeBlood;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getSurgeries() {
        return surgeries;
    }

    public void setSurgeries(String surgeries) {
        this.surgeries = surgeries;
    }

    public String getTransplant() {
        return transplant;
    }

    public void setTransplant(String transplant) {
        this.transplant = transplant;
    }

    public boolean isDonor() {
        return donor;
    }

    public void setDonor(boolean donor) {
        this.donor = donor;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }
}
