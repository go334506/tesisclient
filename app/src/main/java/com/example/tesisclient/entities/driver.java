package com.example.tesisclient.entities;

public class driver {

    String _id;
    String user;
    String name;
    String appat;
    String amat;
    String phone;
    String email;
    String edad;
    int cp;
    String calle;
    String colony;
    String municipality;
    int noext;
    int noint;
    String __v;

    public driver(String _id, String user, String name, String appat, String amat, String phone, String email, String edad, int cp, String calle, String colony, String municipality, int noext, int noint, String __v) {
        this._id = _id;
        this.user = user;
        this.name = name;
        this.appat = appat;
        this.amat = amat;
        this.phone = phone;
        this.email = email;
        this.edad = edad;
        this.cp = cp;
        this.calle = calle;
        this.colony = colony;
        this.municipality = municipality;
        this.noext = noext;
        this.noint = noint;
        this.__v = __v;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppat() {
        return appat;
    }

    public void setAppat(String appat) {
        this.appat = appat;
    }

    public String getAmat() {
        return amat;
    }

    public void setAmat(String amat) {
        this.amat = amat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public int getNoext() {
        return noext;
    }

    public void setNoext(int noext) {
        this.noext = noext;
    }

    public int getNoint() {
        return noint;
    }

    public void setNoint(int noint) {
        this.noint = noint;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }
}
