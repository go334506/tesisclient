package com.example.tesisclient.ui.slideshow;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.tesisclient.R;

public class SlideshowFragment extends Fragment {

    EditText EditTextMatricula,EditTextMarca,EditTextModelCar,EditTextColor,EditTextAnioCar;
    EditText EditTextCountryCar,EditTextNoMotor,EditTextNoChasis;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);

        EditTextMatricula=(EditText) root.findViewById(R.id.EditTextMatricula);
        EditTextMarca=(EditText) root.findViewById(R.id.EditTextMarca);
        EditTextModelCar=(EditText) root.findViewById(R.id.EditTextModelCar);

        EditTextColor=(EditText) root.findViewById(R.id.EditTextColor);
        EditTextAnioCar=(EditText) root.findViewById(R.id.EditTextAnioCar);
        EditTextCountryCar=(EditText) root.findViewById(R.id.EditTextCountryCar);

        EditTextNoMotor=(EditText) root.findViewById(R.id.EditTextNoMotor);
        EditTextNoChasis=(EditText) root.findViewById(R.id.EditTextNoChasis);

        SharedPreferences preferences = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);

        EditTextMatricula.setText(preferences.getString("enrollment",""));
        EditTextMarca.setText(preferences.getString("brand",""));
        EditTextModelCar.setText(preferences.getString("model",""));
        EditTextColor.setText(preferences.getString("color",""));

        //year
        EditTextAnioCar.setText( Integer.toString(preferences.getInt("year",0)));
        EditTextCountryCar.setText(preferences.getString("country",""));
        EditTextNoMotor.setText(preferences.getString("nEngine",""));
        EditTextNoChasis.setText(preferences.getString("nChassis",""));

        Button button = (Button) root.findViewById(R.id.buttonSaveCar);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(getActivity(),"Guardando",Toast.LENGTH_SHORT).show();

                SharedPreferences preferences = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor objEditor = preferences.edit();

                objEditor.putString("enrollment",EditTextMatricula.getText().toString());
                objEditor.putString("brand",EditTextMarca.getText().toString());
                objEditor.putString("model",EditTextModelCar.getText().toString());
                objEditor.putString("color",EditTextColor.getText().toString());

                objEditor.putInt("year",Integer.parseInt(EditTextAnioCar.getText().toString()));
                objEditor.putString("country",EditTextCountryCar.getText().toString());
                objEditor.putString("nEngine",EditTextNoMotor.getText().toString());
                objEditor.putString("nChassis",EditTextNoChasis.getText().toString());

              objEditor.commit();
            }
        });

        return root;
    }


}