package com.example.tesisclient.ui.gallery;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.tesisclient.R;

public class GalleryFragment extends Fragment {

    EditText EditTextSangre,EditTextEnfermedades,EditTextTratamiento,EditTextCirugias,EditTextTrasplante;
    Switch switchTransplant;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_gallery, container, false);


        EditTextSangre=(EditText) root.findViewById(R.id.EditTextSangre);
        EditTextEnfermedades=(EditText) root.findViewById(R.id.EditTextEnfermedades);
        EditTextTratamiento=(EditText) root.findViewById(R.id.EditTextTratamiento);
        EditTextCirugias=(EditText) root.findViewById(R.id.EditTextCirugias);
        EditTextTrasplante=(EditText) root.findViewById(R.id.EditTextTrasplante);
        switchTransplant=(Switch) root.findViewById(R.id.switchTransplant);

        SharedPreferences preferences = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);

        EditTextSangre.setText(preferences.getString("tipeBlood",""));
        EditTextEnfermedades.setText(preferences.getString("disease",""));
        EditTextTratamiento.setText(preferences.getString("treatment",""));

        EditTextCirugias.setText(preferences.getString("surgeries",""));
        EditTextTrasplante.setText(preferences.getString("transplant",""));
        //donor
        switchTransplant.setChecked(preferences.getBoolean("donor",false));

        Button button = (Button) root.findViewById(R.id.buttonSaveMedical);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Toast.makeText(getActivity(),"Guardando",Toast.LENGTH_SHORT).show();

                SharedPreferences preferences = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor objEditor = preferences.edit();

                objEditor.putString("tipeBlood",EditTextSangre.getText().toString());
                objEditor.putString("disease",EditTextEnfermedades.getText().toString());
                objEditor.putString("treatment",EditTextTratamiento.getText().toString());

                objEditor.putString("surgeries",EditTextCirugias.getText().toString());
                objEditor.putString("transplant",EditTextTrasplante.getText().toString());
                if(switchTransplant.isChecked()){
                    objEditor.putBoolean("donor",true);
                }else {
                    objEditor.putBoolean("donor",false);
                }



                objEditor.commit();
            }
        });


        return root;
    }
}