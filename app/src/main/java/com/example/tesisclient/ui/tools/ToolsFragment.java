package com.example.tesisclient.ui.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.tesisclient.R;

public class ToolsFragment extends Fragment {

    EditText EditTextName,EditTextAppat,EditTextAmat,EditTextEdad,EditTextPhone;
    EditText EditTextEmail,EditTextPhoneEmergency,EditTextCp,EditTextMunicipality;
    EditText EditTextNoExt,EditTextNoInt;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_tools, container, false);

        EditTextName=(EditText) root.findViewById(R.id.EditTextName);
        EditTextAppat=(EditText) root.findViewById(R.id.EditTextAppat);
        EditTextAmat=(EditText) root.findViewById(R.id.EditTextAmat);
        EditTextEdad=(EditText) root.findViewById(R.id.EditTextEdad);
        EditTextPhone=(EditText) root.findViewById(R.id.EditTextPhone);

        EditTextEmail=(EditText) root.findViewById(R.id.EditTextEmail);
        EditTextPhoneEmergency=(EditText) root.findViewById(R.id.EditTextPhoneEmergency);
        EditTextCp=(EditText) root.findViewById(R.id.EditTextCp);
        EditTextMunicipality=(EditText) root.findViewById(R.id.EditTextMunicipality);

        EditTextNoExt=(EditText) root.findViewById(R.id.EditTextNoExt);
        EditTextNoInt=(EditText) root.findViewById(R.id.EditTextNoInt);


        SharedPreferences preferences = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);

        EditTextName.setText(preferences.getString("user",""));
        EditTextAppat.setText(preferences.getString("appat",""));
        EditTextAmat.setText(preferences.getString("amat",""));

        EditTextEdad.setText(preferences.getString("edad",""));
        EditTextPhone.setText(preferences.getString("phone",""));
        EditTextPhoneEmergency.setText(preferences.getString("phoneEmergency",""));

        EditTextCp.setText( Integer.toString(preferences.getInt("cp",0)));
        EditTextMunicipality.setText(preferences.getString("municipality",""));

        EditTextNoExt.setText( Integer.toString(preferences.getInt("noext",0)));
        EditTextNoInt.setText( Integer.toString(preferences.getInt("noint",0)));


        return root;
    }
}