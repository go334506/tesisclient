package com.example.tesisclient.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;

import com.example.tesisclient.AlertActivity;
import com.example.tesisclient.CLocation;
import com.example.tesisclient.MainActivity;
import com.example.tesisclient.R;
import com.example.tesisclient.RegisterDriverActivity;
import com.example.tesisclient.ShakeDetector;
import com.example.tesisclient.entities.report;
import com.example.tesisclient.response.reponsePostReport;
import com.example.tesisclient.retrofit.apiRest;
import com.example.tesisclient.retrofit.utilities;
import com.google.gson.Gson;

import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment implements SensorEventListener,LocationListener {

    LinearLayout linearLayout1,linearLayout2;
    TextView textViewX,textViewY,textViewZ,textViewSpeed,textViewInfoActivate,textViewLatitude,textViewLongitude;

    LinearLayout LinearLayout5;
    Switch switchMonitoring;
    SwitchCompat switchMetric;

    // The following are used for the shake detection
    SensorManager sm;
    List<Sensor> sensors;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;


    private apiRest mApiService;
    //variables que se enviaran al service
    public String idUsuario;
    public float nCurrentSpeed;
    public  String unitMetric;
    public double nLatitude,nLongitud;

    public HomeFragment() {

        // Required empty public constructor
    }

    @SuppressLint("WrongConstant")
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        linearLayout1=(LinearLayout) root.findViewById(R.id.linearLayout1);
        linearLayout2=(LinearLayout) root.findViewById(R.id.linearLayout2);
        LinearLayout5=(LinearLayout) root.findViewById(R.id.LinearLayout5);

        textViewInfoActivate=(TextView) root.findViewById(R.id.textViewInfoActivate);
        textViewX=(TextView) root.findViewById(R.id.textViewX);
        textViewY=(TextView) root.findViewById(R.id.textViewY);
        textViewZ=(TextView) root.findViewById(R.id.textViewZ);
        textViewLatitude=(TextView) root.findViewById(R.id.textViewLatitude);
        textViewLongitude=(TextView) root.findViewById(R.id.textViewLongitude);

        switchMonitoring=(Switch) root.findViewById(R.id.switchMonitoring);

        sm=(SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        sensors=sm.getSensorList(Sensor.TYPE_ACCELEROMETER);

        mApiService= utilities.getApiService();

        //ver el id usuario que se guardo
        SharedPreferences preferences = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);
        this.idUsuario= preferences.getString("idUsuario","usuario");

        Toast.makeText(getActivity(),"Usuario: "+idUsuario,Toast.LENGTH_LONG).show();
        //checar si no esta activado
        if(!switchMonitoring.isChecked()){

            linearLayout1.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.VISIBLE);
            textViewInfoActivate.setText("   El Monitoreo permite conexión con\n   el servidor por favor actívala si\n   vas a viajar.");
        }



        if(sensors.size()>0){
            sm.registerListener((SensorEventListener)this,sensors.get(0),SensorManager.SENSOR_DELAY_NORMAL);
        }


        switchMetric = root.findViewById(R.id.switchMetric);
        textViewSpeed = root.findViewById(R.id.textViewSpeed);
        //check for gps permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},100);
        }else{
            //start the program if the permission is granted
            doStuff();
        }

        this.updateSpeed(null);
        this.updateLocation(null);

        switchMetric.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                HomeFragment.this.updateSpeed(null);
            }
        });


        switchMonitoring.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                   // Toast.makeText(getActivity(),"ON",Toast.LENGTH_SHORT).show();
                    switchMonitoring.setText("   Activado");
                    linearLayout1.setVisibility(View.VISIBLE);
                    linearLayout2.setVisibility(View.GONE);
                } else {
                    //Toast.makeText(getActivity(),"OFF",Toast.LENGTH_SHORT).show();
                    switchMonitoring.setText("   Desctivado");
                    linearLayout1.setVisibility(View.GONE);
                    linearLayout2.setVisibility(View.VISIBLE);
                }


            }
        });

        // ShakeDetector initialization

        mAccelerometer = sm
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                    if(switchMonitoring.isChecked()){//si esta activado el switch de monitoreo alertar
                        alertAccident(count);
                    }

                //Toast.makeText(getActivity(),"usuario"+idUsuario ,Toast.LENGTH_LONG).show();
            }
        });



        return root;



    }//fin de onCreate


    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        sm.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        sm.unregisterListener(mShakeDetector);
        super.onPause();
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        String text="   ";
        textViewX.setText(text+"X: "+event.values[SensorManager.DATA_X]);
        textViewY.setText(text+"Y: "+event.values[SensorManager.DATA_Y]);
        textViewZ.setText(text+"Z: "+event.values[SensorManager.DATA_Z]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(location!=null){
            CLocation myLocation = new CLocation(location,this.useMetricUnits());
            this.updateSpeed(myLocation);
            this.updateLocation(myLocation);
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    @SuppressLint("MissingPermission")
    private void doStuff() {
        LocationManager locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        if(locationManager != null){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
        }
        Toast.makeText(getActivity(),"Esperando conexión GPS", Toast.LENGTH_SHORT).show();
    }

    private void updateSpeed(CLocation location){
        this.nCurrentSpeed =0;
        if(location!=null){
            location.setUserMetricUnits(this.useMetricUnits());
            nCurrentSpeed = location.getSpeed();
        }
        Formatter fmt = new Formatter(new StringBuilder());
        fmt.format(Locale.US,"%5.1f",nCurrentSpeed);
        String strCurrentSpeed = fmt.toString();
        strCurrentSpeed =strCurrentSpeed.replace(" ","0");

        //String strUnits
        if(this.useMetricUnits()){
            this.unitMetric="km/h";
            textViewSpeed.setText("   "+strCurrentSpeed+" "+this.unitMetric);

        }else{
            this.unitMetric="millas/h";
            textViewSpeed.setText("   "+strCurrentSpeed+" "+this.unitMetric);
        }

    }

    private void updateLocation(CLocation location){

        this.nLatitude =0;
        this.nLongitud =0;
        if(location!=null){
            nLatitude = location.getLatitude();
            nLongitud = location.getLongitude();
        }

         textViewLatitude.setText("   Latitud:   "+(String.format("%.7f", nLatitude)));
        textViewLongitude.setText("   Longitud: "+(String.format("%.7f", nLongitud)));

    }
    private boolean useMetricUnits(){

        return switchMetric.isChecked();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode ==100){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                this.doStuff();
            }else {
                getActivity().finish();
            }
        }
    }

    public void alertAccident(int count){
        /*
         * The following method, "handleShakeEvent(count):" is a stub //
         * method you would use to setup whatever you want done once the
         * device has been shook.
         */
        Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("¿Estás bien? hemos detectado una colisión")
                .setCancelable(false)
                .setPositiveButton("Necesito ayuda", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        sendReport();
                        //  getActivity().finish();

                    }
                })
                .setNegativeButton("Estoy bien", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

        // Toast.makeText(getActivity(),"Agitación"+count,Toast.LENGTH_LONG).show();
    }

    public void sendReport(){


        mApiService.addReport(new report(this.idUsuario,this.nCurrentSpeed,this.unitMetric,this.nLatitude,this.nLongitud,0,0,0)).enqueue(new Callback<reponsePostReport>() {
            @Override
            public void onResponse(Call<reponsePostReport> call, Response<reponsePostReport> response) {
                Gson objectConsole = new Gson();
                Log.i("pruebaRest",objectConsole.toJson(response.body()));
                int codeResp=Integer.parseInt(objectConsole.toJson(response.body().getStatus().getCode()));
                String tokenResp=objectConsole.toJson(response.body().getStatus().getMessage());

                if(codeResp==1){
                    // aqui debemos guardar el token y los datos que regresan en shared preferences
                  //redireccionar a Main activity
                    Intent miIntent=null;
                    miIntent=new Intent(getActivity(), AlertActivity.class);
                    startActivity(miIntent);
                }else{

                    Toast.makeText(getActivity(),tokenResp,Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<reponsePostReport> call, Throwable t) {

            }
        });
    }

}