package com.example.tesisclient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    public  boolean tokenExists= false;
    private AppBarConfiguration mAppBarConfiguration;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        //datos de configuración
        preferences.getBoolean("tokenExists",false);
        preferences.getString("idUsuario","usuario");

        //datos del conductor
        preferences.getString("user","");
        preferences.getString("name","");
        preferences.getString("appat","");
        preferences.getString("amat","");
        preferences.getString("phone","");
        preferences.getString("email","");
        preferences.getString("edad","");
        preferences.getInt("cp",00000);
        preferences.getString("calle","");
        preferences.getString("colony","");
        preferences.getString("municipality","");
        preferences.getInt("noext",0);
        preferences.getInt("noint",0);

        //datos medicos
        preferences.getString("tipeBlood","");
        preferences.getString("disease","");
        preferences.getString("treatment","");
        preferences.getString("surgeries","");
        preferences.getString("transplant","");
        preferences.getBoolean("donor",false);


        //datos del vehiculo
        preferences.getString("enrollment","");//Matrícula
        preferences.getString("brand","");
        preferences.getString("model","");
        preferences.getString("color","");
        preferences.getInt("year",0);
        preferences.getString("country","");
        preferences.getString("nEngine","");
        preferences.getString("nChassis","");

        //contactos de emergencía
        preferences.getString("userE","");
        preferences.getString("nameE","");
        preferences.getString("appatE","");
        preferences.getString("phoneE","");
        preferences.getString("emailE","");
        preferences.getInt("cpE",00000);
        preferences.getString("calleE","");
        preferences.getString("colony","");
        preferences.getString("municipalityE","");
        preferences.getInt("noextE",0);
        preferences.getInt("nointE",0);



        // si no hay token registrense o loguense
         if(!this.validateToken()) {
            Intent miIntent=null;
            miIntent=new Intent(MainActivity.this,HelloActivity.class);
            if(miIntent!=null){
                startActivity(miIntent);
            }
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);



    }


    public boolean validateToken(){

        SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        this.tokenExists=  preferences.getBoolean("tokenExists",false);
            if(this.tokenExists){
               // Toast.makeText(getApplicationContext(),"Token exists",Toast.LENGTH_LONG).show();
            }else {
               // Toast.makeText(getApplicationContext(),"Token No exists:",Toast.LENGTH_LONG).show();

            }

        return this.tokenExists;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                // aqui debemos guardar el token y los datos que regresan en shared preferences
                SharedPreferences preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor objEditor = preferences.edit();
                objEditor.putBoolean("tokenExists",false);
                objEditor.putString("idUsuario","usuario");
                objEditor.commit();
                Toast.makeText(this,"Cerrando Sesión",Toast.LENGTH_SHORT).show();
                Intent miIntent=null;
                miIntent=new Intent(MainActivity.this,MainActivity.class);
                startActivity(miIntent);

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }



}
